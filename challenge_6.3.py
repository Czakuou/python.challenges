#Challenge 6.3
#This is a script that converts Fahrenheit to Celsius and vice versa.


degree_Celsius = input("Enter a temperature in Celsius degrees: ")
degree_Fahrenheit = input("Enter a temperature in Fahrenheit degrees: ")


def convert_cel_to_far(degree_Celsius):

    Fahrenheit = int(degree_Celsius) * 9/5 + 32
    return f"{degree_Celsius} is {Fahrenheit:.2f} degrees Fahrenheit"


def convert_far_to_cel(degree_Fahrenheit):

    Celsius = (int(degree_Fahrenheit) - 32) * 5/9
    return f"{degree_Fahrenheit} is {Celsius:.2f} degrees Celsius"


if __name__ == '__main__':
    print(convert_cel_to_far(degree_Celsius))
    print(convert_far_to_cel(degree_Fahrenheit))
