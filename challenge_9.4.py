#Challenge 9.4
#Challenge : List of Lists
import statistics

universities = [
['California Institute of Technology', 2175, 37704],
['Harvard', 19627, 39849],
['Massachusetts Institute of Technology', 10566, 40732],
['Princeton', 7802, 37000],
['Rice', 5879, 35551],
['Stanford', 19535, 40569],
['Yale', 11701, 40500]
]


#funkcja obliczjąca studentów i czesne
def enrollment_stats(universities):
    total_students = []
    total_tuition = []
    for count in universities:
        total_students.append(count[1])
        total_tuition.append(count[2])

    return total_students, total_tuition


#funkcja obliczająca średnią
def mean(element_to_mean):
    element_to_mean = sum(element_to_mean) / len(element_to_mean)
    return element_to_mean


#funckcja obliczająca medianę
def median(element_to_median):
    return statistics.median(element_to_median)

#przypisanie do zmienniej "podsumowanie" - zmiana estetyczna
summary = enrollment_stats(universities)

print("*" * 30)
print(f"Total students:   {sum(summary[0]):,}")
print(f"Total tuition:   {sum(summary[1]):,} $")
print(f"\nStudent mean:    {mean(summary[0]):,.2f}")
print(f"Student median:   {median(summary[0]):,}")
print(f"\nTuition mean:    {mean(summary[1]):,.2f} $")
print(f"Tuition median:  {median(summary[1]):,} $")
print("*" * 30)
