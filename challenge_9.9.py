#Challenge 9.9
#Cats with Hats

num_of_cats = 100
cats_with_hats = []
num_of_laps = 100


for lap in range(1, num_of_laps + 1):
    for cat in range(1, num_of_cats + 1):

        
        if cat % lap == 0:
            if cat in cats_with_hats:
                cats_with_hats.remove(cat)
            else:
                cats_with_hats.append(cat)

print(cats_with_hats)