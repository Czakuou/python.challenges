# Challenge 8.8 
# Challenge: Simulate a Coin Toss Experiment

import random

def coin_flip():
    return random.choice(["head", "tails"])

def coin_experiment(trials_num):
    flips = 0
    first_result = None
    for _ in range(trials_num):
        first_result = coin_flip()
        flips += 1
        while coin_flip() == first_result:
            flips += 1
        flips += 1
    return flips / trials_num


def main():
    print(coin_experiment(10_000))

if __name__ == '__main__':
    main()