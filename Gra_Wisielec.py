#Gra w Wisielca 

import random, time
#uzyte moduly: random - wybór losowego hasła, time: opóźnienie pojawianie się printów

#lista drukująca wisielca
wisielec = ['''
   +---+
       |
       |
       |
      ===''', '''
   +---+
   O   |
       |
       |
       ===''', '''
   +---+
   O   |
   |   |
       |
      ===''', '''
   +---+
   O   |
  /|   |
       |
      ===''', '''
   +---+
   O   |
  /|\  |
       |
      ===''', '''
   +---+
   O   |
  /|\  |
  /    |
      ===''', '''
   +---+
   O   |
  /|\  |
  / \  |
      ===''']

#Powitanie i opis gry
print("Witam Cię w grze Wisielec")
time.sleep(2.0)
user_name = input("Wpisz proszę jak mam się do Ciebie zwracać: ")
time.sleep(1.0)
print(f"Zatem {user_name}, hasłem jest losowo wybierany kolor. Postaraj się go odgądnąć, litera po literze")
time.sleep(2.5)
print("W przeciwnym razie na szubienicy zawiśnie przypadkowy człowiek :( ")
time.sleep(2.5)
print(" ")
print(f"Po każdej nietrafionej literze, na szubienicy dojdzie kolejny element\nGra się toczy do momentu, gdy wisielec zawiśnie :/\nOby do tego nie doszło!\n")
print(f"Hasła zawierają wyłącznie małe litery. Nie zawierają cyfr\nPowodzenia {user_name} !")
time.sleep(8.0)
print('Oto szubienica: ')
print(wisielec[0])
time.sleep(2)

def wisielec_gra(): #gra w wisielca


    n = 0 #wartosc początkowa listy wisielca
    lista_hasel = ["bialy", "czarny", "zielony", "rozowy", "fioletowy", "krwisty", "zolty", "czerwony", "antracytowy"]
    haslo = random.choice(lista_hasel) #losowo dobierane hasło z listy
    odgadywane_haslo = ["_"] 
    odgadywane_haslo = len(haslo) * ["_"] #lista, którą uzupelnia user wpisując literę
    litera = [] #litera wpisywana przez usera i umieszczana w "odgadywanym haśle"
    print(" ")
    print("Oto długość Twojego hasła. Każda kreska: _ odpowiada jednej literze.")
    print(odgadywane_haslo)
    time.sleep(1)

    while list(haslo) != odgadywane_haslo: # przerywa pętle, gdy odgadywane haslo != losowe haslo
        time.sleep(1)
        print("Wpisz literę z hasła: ")
        litera = input()
        if n == 5: # przerwanie pętli - wisielec zawisnął
            print(wisielec[6])
            break
        elif litera in haslo: #sprawdza czy litera jest w haśle i uzupelnia o nią "odgadywane haslo"
            for i in range(len(haslo)):
                if (haslo[i] == litera):
                    odgadywane_haslo[i] = litera
            print(f"Ta litera jest w haśle")
            print(odgadywane_haslo)
        else:
            print("Tej litery nie ma w haśle")
            n = n + 1
            print(wisielec[n])


    time.sleep(1.5)

    if list(haslo) == odgadywane_haslo:
        print(" ")
        print(f'Bravo {user_name} zrobiłeś to!\nHasłem było słowo  "{haslo}"\nGratulacje!')

    else:
        print(" ")
        print(f"{user_name} niestety Ci się nie udało.\nHasłem było {haslo}\nEgzekucja została dokonana!\nSpróbuj ponownie!")
 
        
if __name__ == '__main__':
    wisielec_gra()





