# Chalenge 8.4 
# Challenge: Find the Factors of a Number

#Check if user input is integrer > 0
while True: 
    try:
        answer = int(input("Wpisz cyfrę całkowitą, dodatnią, większą od zera: "))
        if answer < 0 or answer == 0:
            print("Źle wpisałeś liczbę")
            continue
        elif answer > 0: 
            break
    except ValueError:
        print("Wpisana liczba nie jest liczbą całkowitą")
    
#Loop which is finding factors of user input
for liczba in range(1, answer + 1): #
    if answer % liczba == 0:
        print(f'{liczba} is a factor of {answer}')