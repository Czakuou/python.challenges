#Challenge 6.5
#This program is tracking your investments


def invest(amount, rate, years):
    """This function show growth 
       of your wallet year by year"""

    for year in range(1, years + 1):
        amount = amount + amount * rate
        print(f"Year {year}: ${amount: ,.2f}")
        

amount = float(input("What is your principal amount?: "))
rate = float(input("What is annual rate of return?: "))
years = int(input("How many years you want to calculate?: "))


if __name__ == "__main__":
    invest(amount, rate, years)



