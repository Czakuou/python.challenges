# Challenge 8.9
# Challenge: Simulate an Election

from random import random

#Basic data: Total Election Wins and nr of trials
kaczor_wins = 0
tuskeł_wins = 0
num_of_times = 10_000

for _ in range(num_of_times):
    #votes in 1 election
    kaczor_votes = 0
    tuskeł_votes = 0

    if random() < .87: # Single election in Region 1
        kaczor_votes += 1
    else:
        tuskeł_votes += 1

    if random() < .65: # Single election in Region 2
        kaczor_votes += 1
    else:
        tuskeł_votes += 1

    if random() < .17: # Single election in Region 3
        kaczor_votes += 1
    else:
        tuskeł_votes += 1

    if kaczor_votes > tuskeł_votes: # Adding single result to total result
        kaczor_wins += 1
    else:
        tuskeł_wins += 1


def result_print():
    print(f"Probability of Kaczor wins: {(kaczor_wins / num_of_times):,.2f} %")
    print(f"Probability of Tuskeł wins: {(tuskeł_wins / num_of_times):,.2f} %")

if __name__ == "__main__":
    result_print()
