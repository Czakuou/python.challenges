# Chapter 5.3 Challenge:Perform Calculations on User Input
print("Enter a base")
base = float(input())

print("Enter a exponent")
exponent = int(input())

print(f"{base} to the power of {exponent} = {base ** exponent}")